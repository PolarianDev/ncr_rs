use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let n = args[1].parse::<u64>().unwrap();
    let r = args[2].parse::<u64>().unwrap();
    println!("---- ncr calculator ----");
    println!("{}C{} = {}", n, r, ncr(n, r).unwrap());
}

fn ncr(n: u64, r: u64) -> Option<u64> {
    Some((factorial(n).unwrap() / (factorial(r).unwrap() * factorial(n - r).unwrap())) as u64)
}

fn factorial(num: u64) -> Option<u128> {
    let mut result: u128 = 1;
    for i in 1..num + 1 {
        result *= i as u128;
    }
    Some(result)
}
